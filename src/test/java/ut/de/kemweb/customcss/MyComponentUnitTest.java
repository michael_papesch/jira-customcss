package ut.de.kemweb.customcss;

import org.junit.Test;
import de.kemweb.customcss.api.MyPluginComponent;
import de.kemweb.customcss.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}